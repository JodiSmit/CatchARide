//
//  RidesTableViewCell.swift
//  CatchARide
//
//  Created by Jodi Lovell on 11/6/17.
//  Copyright © 2017 Smitty. All rights reserved.
//

import UIKit

class RidesTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()

    }
	
	@IBOutlet weak var productName: UILabel?
	@IBOutlet weak var productLogo: UIImageView!
	@IBOutlet weak var rideDetails: UILabel!

	override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
