//
//  ViewControllerHelper.swift
//  CatchARide
//
//  Created by Jodi Lovell on 12/7/17.
//  Copyright © 2017 Smitty. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import GooglePlaces
import GoogleMaps
import MapKit


extension UIViewController {
	
	//MARK: - Activity Indicator management
	func showActivityIndicator() {
		DispatchQueue.main.async {
			let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
			activityIndicator.backgroundColor = UIColor(red:0.16, green:0.17, blue:0.21, alpha:0.5)
			activityIndicator.layer.cornerRadius = 6
			activityIndicator.center = self.view.center
			activityIndicator.hidesWhenStopped = true
			activityIndicator.activityIndicatorViewStyle = .whiteLarge
			activityIndicator.startAnimating()
			//UIApplication.shared.beginIgnoringInteractionEvents()
			
			activityIndicator.tag = 100
			
			for subview in self.view.subviews {
				if subview.tag == 100 {
					print("already added")
					return
				}
			}
			
			self.view.addSubview(activityIndicator)
		}
	}
	
	func hideActivityIndicator() {
		DispatchQueue.main.async {
			let activityIndicator = self.view.viewWithTag(100) as? UIActivityIndicatorView
			activityIndicator?.stopAnimating()
			
			//UIApplication.shared.endIgnoringInteractionEvents()
			activityIndicator?.removeFromSuperview()
			
		}
	}
	
	
	//MARK: - Keyboard management
	func setupViewResizerOnKeyboardShown() {
		NotificationCenter.default.addObserver(self,
											   selector: #selector(keyboardWillShowForResizing),
											   name: Notification.Name.UIKeyboardWillShow,
											   object: nil)
		NotificationCenter.default.addObserver(self,
											   selector: #selector(keyboardWillHideForResizing),
											   name: Notification.Name.UIKeyboardWillHide,
											   object: nil)
	}
	
	@objc func keyboardWillShowForResizing(notification: Notification) {
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
			let window = self.view.window?.frame {
			// We're not just minusing the kb height from the view height because
			// the view could already have been resized for the keyboard before
			self.view.frame = CGRect(x: self.view.frame.origin.x,
									 y: self.view.frame.origin.y,
									 width: self.view.frame.width,
									 height: window.origin.y + window.height - keyboardSize.height)
		}
	}
	
	@objc func keyboardWillHideForResizing(notification: Notification) {
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
			let viewHeight = self.view.frame.height
			self.view.frame = CGRect(x: self.view.frame.origin.x,
									 y: self.view.frame.origin.y,
									 width: self.view.frame.width,
									 height: viewHeight + keyboardSize.height)
		}
	}
	
	func unsubscribeFromKeyboardNotifications() {
		NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
		NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
	}
	
	func dropOffLocation(_ address: String, completion: @escaping (_ result: AnyObject?, _ error: NSError?) -> Void) {
		let geocoder = CLGeocoder()
		geocoder.geocodeAddressString(address) { (placemarks, error) in
			self.completeForwardGeocoding(withPlacemarks: placemarks, error: error)
			
			completion(SearchForRidesController.dropoffLocation, nil)
		}
	}
	
	//MARK: - Geocoding functions
	func completeReverseGeocoding(withPlacemarks placemarks: [CLPlacemark]?, error: Error?) {
		
		if let error = error {
			print("Unable to Reverse Geocode Location (\(error))")
			
		} else {
			if let placemarks = placemarks, let placemark = placemarks.first {
				SearchForRidesController.userLocation = placemark.compactAddress
			} else {
				print("No Matching Addresses Found")
			}
		}
	}
	
	func completeForwardGeocoding(withPlacemarks placemarks: [CLPlacemark]?, error: Error?) {
		
		if let error = error {
			print("Unable to Forward Geocode Address (\(error))")
			print("Unable to Find Location for Address")
			
		} else {
			var location: CLLocation?
			
			if let placemarks = placemarks, placemarks.count > 0 {
				location = placemarks.first?.location
			}
			
			if let location = location {
				SearchForRidesController.dropoffLocation = location
			} else {
				print("No Matching Location Found")
			}
		}
	}
	
}

extension CLPlacemark {
	
	var compactAddress: String? {
		if let name = name {
			var result = name
			if let street = thoroughfare {
				result += ", \(street)"
			}
			if let city = locality {
				result += ", \(city)"
			}
			if let country = country {
				result += ", \(country)"
			}
			return result
		}
		return nil
	}
}
