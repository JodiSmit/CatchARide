//
//  Products.swift
//  CatchARide
//
//  Created by Jodi Lovell on 4/12/18.
//  Copyright © 2018 Smitty. All rights reserved.
//


import Foundation

public struct CARProducts {
	
	public static let UnlockFullVersion = "com.spicypurrito.CatchARide.UnlockFullVersion"

	
	fileprivate static let productIdentifiers: Set<ProductIdentifier> = [CARProducts.UnlockFullVersion]
	
	public static let store = IAPHelper(productIds: CARProducts.productIdentifiers)
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
	return productIdentifier.components(separatedBy: ".").last
}
