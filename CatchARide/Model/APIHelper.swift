//
//  TableViewDataSource.swift
//  CatchARide
//
//  Created by Jodi Lovell on 10/18/17.
//  Copyright © 2017 Smitty. All rights reserved.
//

import Foundation
import UIKit

struct Rides {
	var productName: String
	var productId: String
	var timeEstimate: Int
	var priceEstimate: Int
}


enum UberRideNames: String {
	case uberX = "uberX"
	case uberXl = "uberXL"
	case uberSelect = "SELECT"
	case uberPool = "POOL"
	case uberBlack = "BLACK"
	case uberSuv = "SUV"
	case uberAssist = "ASSIST"
	case uberWav = "WAV"
	case uberExpressPool = "EXPRESS POOL"
}


enum LyftRideNames: String {
	case lyft = "lyft"
	case lyftLine = "lyft_line"
	case lyftPlus = "lyft_plus"
	case lyftPremier = "lyft_premier"
	case lyftLux = "lyft_lux"
	case lyftLuxSuv = "lyft_luxsuv"
	
}


public func uberRideNames(productName: String) -> String {
	if let product = UberRideNames(rawValue: productName) {
		switch product {
		case .uberX:
			return "UberX"
		case .uberXl:
			return "UberXL"
		case .uberSelect:
			return "UberSelect"
		case .uberPool:
			return "UberPOOL"
		case .uberBlack:
			return "UberBLACK"
		case .uberSuv:
			return "UberSUV"
		case .uberAssist:
			return "UberAssist"
		case .uberWav:
			return "UberWAV"
		case .uberExpressPool:
			return "UberExpress POOL"
		}
	} else {
		print("Couldn't find a product name \(productName)")
		return ""
	}
}


public func lyftRideNames(productName: String) -> String {
	
	if let product = LyftRideNames(rawValue: productName) {
		switch product {
		case .lyft:
			return "Lyft"
		case .lyftLine:
			return "Lyft Line"
		case .lyftPlus:
			return "Lyft Plus"
		case .lyftPremier:
			return "Lyft Premier"
		case .lyftLux:
			return "Lyft Lux"
		case .lyftLuxSuv:
			return "Lyft Lux SUV"
		}
	} else {
		print("Couldn't find a product name \(productName)")
		return ""
	}
}


