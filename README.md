# Description
The CatchARide app is a fairly simple app that allows the user to compare price and driver distances (time estimates) for the two most popular ride sharing apps - Uber and Lyft. The user can also save addresses to their address book and will be redirected to the Uber or Lyft app depending on which ride they have selected. There is a requirement that the user have the destination app installed and they will be redirected to the app store if the app is not found on the device.

## How to Build
1. This app requires the use of Cocoapods in order to include the relevant libraries. 
2. The podfile is included in the repo and will require installation prior to opening the project. 
3. Bear in mind that you will need to use the CatchARide.workspace icon to open the project following installation of the Cocoapod(s).

## How to Use
The process flow is as follows:
1. The user is initially shown a screen with a map showing their location as well as a search bar to search for an address and a button which will allow the user to Search the Address Book.
2. If the user taps on the search bar to search for an address, a GooglePlaces tableview will appear with autocomplete functionality to allow for a quick search.
3. If the user picks a correct address in the GooglePlaces tableview, they will be given the option to either search immediately or save the address. If they select to save the address, they will be prompted to provide a name and the app will then automatically perform a search for rides after the address is saved. They can also cancel to be returned to the search screen (Step 1).
4. The next screen provides a tableview detailing the results of the search which will be available rides with estimated price and driver distance for each ride type. The user has the option to sort the list based on either the price or the distance - the default is by price. At this point, the user can cancel the request to go back to the search screen (step 1) or they can select a ride.
5. If the user selects a ride, they will be redirected to the Uber or Lyft app with all the pertinent parameters being included - their next step would be simply to select the option to book the ride.
6. If at step 2 above, the user chose to "Search the Address Book", they would be directed to another tableview but this one detailing their saved addresses. Here the user has the option to select an address as the destination by tapping on it, to delete an individual address by swiping it, or to delete all addresses using the button at the top right hand side. The user can also select the Cancel button to return to the search screen (step 1).
7. If the user selects an address as the destination, the search for rides will occur as per steps 4 and 5 above.
8. The app will check for network connectivity at steps 2 and 6 which will inform the user if the device is not connected to the network and will not allow the user to continue.

## Thanks to
Icons made by [Freepik](http://www.freepik.com) from [Flaticon](www.flaticon.com) and is licensed by [Creative Commons BY 3.0](http://creativecommons.org/licenses/by/3.0/)

	
	


