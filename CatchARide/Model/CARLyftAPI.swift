//
//  LyftAPI.swift
//  CatchARide
//
//  Created by Jodi Lovell on 9/22/17.
//  Copyright © 2017 Smitty. All rights reserved.
//

import Foundation
import CoreLocation
import LyftSDK

//MARK: - This file manages API functions relating to Lyft

class CARLyftAPI: NSObject {
	
	func getLyftEstimate(_ pickupCoordinates: CLLocationCoordinate2D, _ dropOffCoordinates: CLLocationCoordinate2D, completion: @escaping (_ result: Bool, _ error: NSError?) -> Void) {
		
		let currencyFormatter = NumberFormatter()
		currencyFormatter.usesGroupingSeparator = true
		currencyFormatter.numberStyle = NumberFormatter.Style.currency
		currencyFormatter.locale = NSLocale.current
		
		var productName = ""
		let pickup = pickupCoordinates
		let dropoff = dropOffCoordinates
		var lyftError: NSError? = nil
		
		LyftAPI.ETAs(to: pickup) { etaResult in
			if (etaResult.error != nil) {
				lyftError = NSError(domain: "", code: 10, userInfo: [NSLocalizedDescriptionKey : "We had a problem connecting to Lyft. Please try again."])
				completion(false, lyftError)
				return
			}
			let etaCount = etaResult.value?.count
			if (etaCount == 0) {
				lyftError = NSError(domain: "", code: 10, userInfo: [NSLocalizedDescriptionKey : "It looks like there are no Lyft cars in your area. Please check that Lyft operates in your region before trying your search again!"])
				completion(false, lyftError)
				return
			}
			etaResult.value?.forEach { eta in
				LyftAPI.costEstimates(from: pickup, to: dropoff, rideKind: eta.rideKind) { costResult in
					if (costResult.error != nil) {
						lyftError = NSError(domain: "", code: 10, userInfo: [NSLocalizedDescriptionKey : "We had a problem connecting to Lyft. Please try again."])
						completion(false, lyftError)
						return
					}
					costResult.value?.forEach { cost in
						let price = cost.estimate?.maxEstimate.amount
						let priceString = currencyFormatter.string(from: price! as NSNumber)
						let priceInt = Int(truncating: currencyFormatter.number(from: priceString!)!)

						productName = lyftRideNames(productName: eta.rideKind.rawValue)
						RidesResultTableViewController.rideEstimates.append(Rides(productName: productName, productId: eta.rideKind.rawValue, timeEstimate: eta.minutes, priceEstimate: priceInt))
						
						let count = RidesResultTableViewController.rideEstimates.count
						if etaCount == count {
							print("Lyft Completion True")
							completion(true,nil)
						}
					}
				}
			}
		}
	}
}
