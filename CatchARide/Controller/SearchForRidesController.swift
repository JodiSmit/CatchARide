//
//  SearchForRidesController.swift
//  CatchARide
//
//  Created by Jodi Lovell on 9/21/17.
//  Copyright © 2017 Smitty. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import CoreData
import GooglePlaces
import GoogleMaps
import GoogleMobileAds


class SearchForRidesController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UISearchBarDelegate,UISearchDisplayDelegate, GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate {
	
	var addresses: [NSManagedObject] = []
	var dropOffAddress: String = ""
	var currentLocation: CLLocation? = nil
	static var dropoffLocation: CLLocation? = nil
	static var userLocation: String? = nil
	let lyftAPI = CARLyftAPI()
	let uberAPI = CARUberAPI()
	static var rideEstimates = [Rides]()
	let locManager = CLLocationManager()
	
	let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
	
	
	@IBOutlet weak var loadingImage: UIImageView!
	@IBOutlet weak var bannerView: GADBannerView!
	@IBOutlet weak var bottomView: UIView!
	@IBOutlet weak var addressBookButton: UIButton!
	@IBOutlet weak var addressSearch: UISearchBar!
	@IBOutlet weak var mapView: MKMapView!
	@IBOutlet weak var bottomStack: UIStackView!
	
	
	override func viewWillAppear(_ animated: Bool) {
		
		
		if UserDefaults.standard.bool(forKey: CARProducts.UnlockFullVersion) == false {
			
			//MARK: - AdMob Banner Ad setup
			let adRequest = GADRequest()
			adRequest.testDevices = [ kGADSimulatorID, "F706B123-CE03-4738-905D-90A726F733A7", "44192125bbdea06b911d8a2f160fec4e" ]
			bannerView.adUnitID = "ca-app-pub-7151213256097128/3803229784"
			//bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716" //TEST ADS
			bannerView.rootViewController = self
			bannerView.load(GADRequest())
		} else {
			self.navigationController?.setNavigationBarHidden(true, animated: true)
			bannerView.isHidden = true
		}
	}
	override func viewDidLoad() {
		super.viewDidLoad()
		
		bottomView.isHidden = false
		addressSearch.isHidden = false
		bottomStack.isHidden = false
		addressSearch.delegate = self
		mapView.delegate = self
		locManager.delegate = self
		mapView.isUserInteractionEnabled = false
		loadingImage.isHidden = true
		
		locManager.requestWhenInUseAuthorization()
		if CLLocationManager.locationServicesEnabled() {
			locManager.desiredAccuracy = kCLLocationAccuracyBest
			locManager.startUpdatingLocation()
		}
	}
	
	@IBAction func openPurchases(_ sender: Any) {
		self.performSegue(withIdentifier: "UpgradePage", sender: self)
	}
	
	//MARK: - Get Current Location Data
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		let geocoder = CLGeocoder()
		if let location = locations.first {
			locManager.stopUpdatingLocation()
			let annotation = MKPointAnnotation()
			annotation.title = "You are here!"
			self.currentLocation = location
			
			geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
				self.completeReverseGeocoding(withPlacemarks: placemarks, error: error)
			}
			self.dropPinInformation(annotation: annotation, coordinates: location.coordinate)
		}
	}
	
	func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
		if Reachability.isConnectedToNetwork(){
			let northRegion = CLLocationCoordinate2DMake((locManager.location?.coordinate.latitude)!*1.0001, (locManager.location?.coordinate.longitude)!*1.01)
			let southRegion = CLLocationCoordinate2DMake((locManager.location?.coordinate.latitude)!*0.99, (locManager.location?.coordinate.longitude)!*0.9999)
			
			let bounds = GMSCoordinateBounds(coordinate: northRegion, coordinate: southRegion)
			
			let autoCompleteController = GMSAutocompleteViewController()
			autoCompleteController.delegate = self
			autoCompleteController.autocompleteBounds = bounds
			self.dismiss(animated: true, completion: nil)
			self.present(autoCompleteController, animated: true, completion: nil)
		}else{
			self.showErrorAlert(self.addressSearch, message: "Looks like you don't have Internet connectivity. Please connect and try again.", code: 0)
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "RideEstimates" {
			let svc = segue.destination as? UINavigationController
			let ridesResultVC: RidesResultTableViewController = svc?.topViewController as! RidesResultTableViewController
			
			ridesResultVC.pickupLocation = self.currentLocation
			ridesResultVC.dropOffLocation = SearchForRidesController.dropoffLocation
			
		} else {
			return
		}
	}
	
	@IBAction func addressBookButtonClicked(_ sender: Any) {
		if Reachability.isConnectedToNetwork(){
			self.dismiss(animated: true, completion: nil)
			performSegue(withIdentifier: "AddressBook", sender: UIButton.self)
		} else {
			self.showErrorAlert(self.addressSearch, message: "Looks like you don't have Internet connectivity. Please connect and try again.", code: 0)
		}
	}
	
	//MARK: - Unwind Segue for Address Book Search
	@IBAction func afterSelectingSavedAddressViewController(_ segue: UIStoryboardSegue) {
		bottomView.isHidden = true
		addressSearch.isHidden = true
		bottomStack.isHidden = true
		mapView.isHidden = true
		mapView.isUserInteractionEnabled = false
		loadingImage.image = UIImage(named: "LoadingImageNew")
		loadingImage.isHidden = false
		
		showActivityIndicator()
		searchForRides(address: dropOffAddress)
	}
	
	//MARK: - Searching for available rides
	public func searchForRides(address: String) {
		bottomView.isHidden = true
		addressSearch.isHidden = true
		bottomStack.isHidden = true
		mapView.isHidden = true
		mapView.isUserInteractionEnabled = false
		loadingImage.image = UIImage(named: "LoadingImageNew")
		loadingImage.isHidden = false
		self.navigationController?.setNavigationBarHidden(true, animated: true)
		self.addressSearch.text = ""
		showActivityIndicator()
		
		
		dropOffLocation(address) { (dropResult, dropError) in
			if dropError == nil {
				let dispatchGroup = DispatchGroup()
				print(Date())
				
				dispatchGroup.enter()
				self.uberAPI.getUberEstimate((self.currentLocation?.coordinate)!,(SearchForRidesController.dropoffLocation?.coordinate)!) { (uberResult, uberError) in
					if uberError == nil {
						print("Uber \(uberResult)")
					} else {
						print(uberError?.domain as Any)
						self.showErrorAlert(self.addressSearch, message: (uberError?.localizedDescription)!, code: (uberError?.code)!)
					}
					dispatchGroup.leave()
				}

				
				dispatchGroup.enter()
				self.lyftAPI.getLyftEstimate((self.currentLocation?.coordinate)!,(SearchForRidesController.dropoffLocation?.coordinate)!) { (lyftResult, lyftError) in
					if lyftError == nil {
						print("Lyft \(lyftResult)")
					} else {
						print(lyftError?.domain as Any)
						self.showErrorAlert(self.addressSearch, message: (lyftError?.localizedDescription)!, code: (lyftError?.code)!)
					}
					dispatchGroup.leave()
				}
				
				
				dispatchGroup.notify(queue: .main) {
					print("Both functions complete 👍")
					print(Date())
					self.performSegue(withIdentifier: "RideEstimates", sender: self)
					
					DispatchQueue.main.async {
						if UserDefaults.standard.bool(forKey: CARProducts.UnlockFullVersion) == false {
							self.navigationController?.setNavigationBarHidden(false, animated: true)
						}
						self.bottomView.isHidden = false
						self.addressSearch.isHidden = false
						self.bottomStack.isHidden = false
						self.mapView.isHidden = false
						self.loadingImage.isHidden = true
						self.hideActivityIndicator()
					}
				}
				
			} else {
				print(dropError?.domain as Any)
				self.showErrorAlert(self.addressSearch, message: "We had a problem finding your destination address! Please try again.", code: (dropError?.code)!)
			}
		}
	}
	
	
	// MARK: -  Error alert setup
	func showErrorAlert(_ sender: Any, message: String, code: Int) {
		let errMessage = message
		let alert = UIAlertController(title: nil, message: errMessage, preferredStyle: UIAlertControllerStyle.alert)
		alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
			alert.dismiss(animated: true, completion: nil)
			if code == 10 {
				DispatchQueue.main.async {
					if UserDefaults.standard.bool(forKey: CARProducts.UnlockFullVersion) == false {
						self.navigationController?.setNavigationBarHidden(false, animated: true)
					}
					self.bottomView.isHidden = false
					self.addressSearch.isHidden = false
					self.bottomStack.isHidden = false
					self.mapView.isHidden = false
					self.loadingImage.isHidden = true
					self.hideActivityIndicator()
				}
			}
		}))
		self.present(alert, animated: true, completion: nil)
	}
	
	//MARK: - Save functions
	func saveAddressPopup() {
		let alert = UIAlertController(title: "Save Address", message: "Please enter a name:", preferredStyle: .alert)
		
		let saveAction = UIAlertAction(title: "Save", style: .default) {
			[unowned self] action in
			
			guard let textField = alert.textFields?.first, let addressToSave = textField.text else {
				return
			}
			
			self.saveAddress(name: addressToSave,  addressString: self.dropOffAddress)
		}
		
		let saveAndSearchAction = UIAlertAction(title: "Save and Search", style: .default) {
			[unowned self] action in
			
			guard let textField = alert.textFields?.first, let addressToSave = textField.text else {
				return
			}
			
			self.saveAndSearchAddress(name: addressToSave,  addressString: self.dropOffAddress)
		}
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .default)
		
		alert.addTextField()
		
		alert.addAction(saveAction)
		alert.addAction(saveAndSearchAction)
		alert.addAction(cancelAction)
		
		present(alert, animated: true)
		
	}
	
	func generalSaveAddress(name: String, addressString: String) {
		
		let entity = NSEntityDescription.entity(forEntityName: "AddressBook", in: managedContext)!
		let address = NSManagedObject(entity: entity, insertInto: managedContext)
		managedContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
		
		
		address.setValue(name, forKey: "name")
		address.setValue(addressString, forKey: "address")
		
		do {
			try managedContext.save()
			addresses.append(address)
		} catch let error as NSError {
			print("Could not save. \(error), \(error.userInfo)")
		}
	}
	
	func saveAddress(name: String, addressString: String) {
		
		generalSaveAddress(name: name, addressString: addressString)
		
		let alert = UIAlertController(title: "Saved!", message: "Address has been saved.", preferredStyle: .alert)
		self.present(alert, animated: true, completion: nil)
		let when = DispatchTime.now() + 1.5
		DispatchQueue.main.asyncAfter(deadline: when){
			alert.dismiss(animated: true, completion: nil)
		}
		self.addressSearch.text = ""
	}
	
	func saveAndSearchAddress(name: String, addressString: String) {
		
		generalSaveAddress(name: name, addressString: addressString)
		
		let alert = UIAlertController(title: "Saved!", message: "Please wait while we search for a ride.", preferredStyle: .alert)
		self.present(alert, animated: true, completion: nil)
		let when = DispatchTime.now() + 1.5
		DispatchQueue.main.asyncAfter(deadline: when){
			self.searchForRides(address: addressString)
			alert.dismiss(animated: true, completion: nil)
		}
	}
	
	// MARK: - MapView functions
	func dropPinInformation(annotation: MKPointAnnotation, coordinates: CLLocationCoordinate2D) {
		annotation.coordinate = coordinates
		let regionRadius: CLLocationDistance = 1000
		let coordinateRegion = MKCoordinateRegionMakeWithDistance(annotation.coordinate, regionRadius, regionRadius)
		
		mapView.setRegion(coordinateRegion, animated: true)
		mapView.addAnnotation(annotation)
	}
	
	
	func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
		
		let reuseId = "pin"
		var pinView: MKMarkerAnnotationView
		if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
			as? MKMarkerAnnotationView {
			dequeuedView.annotation = annotation
			pinView = dequeuedView
		} else {
			pinView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
			
		}
		pinView.markerTintColor = .black
		pinView.animatesWhenAdded = true
		pinView.titleVisibility = .visible
		return pinView
	}
	
	//MARK: - GooglePlaces address autocomplete functions.

	func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
		self.addressSearch.text = place.formattedAddress
		self.dropOffAddress = self.addressSearch.text!
		self.dismiss(animated: true, completion: nil)
		self.afterAutoComplete()
	}
	
	func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
		let alert = UIAlertController(title: "Ooops!", message: "Something went wrong, please try again!", preferredStyle: .alert)
		self.dismiss(animated: true, completion: nil)
		self.present(alert, animated: true, completion: nil)
		let when = DispatchTime.now() + 1.5
		DispatchQueue.main.asyncAfter(deadline: when){
			alert.dismiss(animated: true, completion: nil)
		}
		
	}
	
	func wasCancelled(_ viewController: GMSAutocompleteViewController) {
		print("Autocomplete was cancelled.")
		self.dismiss(animated: true, completion: nil)
		self.addressSearch.text = ""
	}
	
	func afterAutoComplete() {
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
		let entity = NSEntityDescription.entity(forEntityName: "AddressBook", in: managedContext)!
		
		fetchRequest.entity = entity
		
		let alertController = UIAlertController(title: "Search or Save?", message: "Click OK to search for rides to this destination.", preferredStyle: UIAlertControllerStyle.alert)
		
		DispatchQueue.main.async {
			
			let saveAction = UIAlertAction(title: "Save", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
				do {
					let result = try self.managedContext.fetch(fetchRequest)
					
					if UserDefaults.standard.bool(forKey: CARProducts.UnlockFullVersion) {
						self.saveAddressPopup()
					} else {
						
						if result.count < 2 {
							
							self.saveAddressPopup()
							
						} else {
							
							let upgradeAlert = UIAlertController(title: "Upgrade?", message: "You have reached your address limit for the free version. Please upgrade for more!", preferredStyle: UIAlertControllerStyle.alert)
							let upgradeAction = UIAlertAction(title: "Upgrade now", style: .default) {
								[unowned self] action in
								
								self.performSegue(withIdentifier: "UpgradePage", sender: self)
							}
							
							let cancelAction = UIAlertAction(title: "Cancel", style: .default)
							
							upgradeAlert.addAction(upgradeAction)
							upgradeAlert.addAction(cancelAction)
							
							self.present(upgradeAlert, animated: true)
							self.addressSearch.text = ""
						}
					}
					
					
				} catch {
					let fetchError = error as NSError
					print(fetchError)
				}
				
			}
			let searchAction = UIAlertAction(title: "Search", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
				self.searchForRides(address: self.dropOffAddress)
				
			}
			alertController.addAction(saveAction)
			alertController.addAction(searchAction)
			self.present(alertController, animated: true, completion: nil)
			
		}
		
	}
	
	// Turn the network activity indicator on and off again.
	func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
		UIApplication.shared.isNetworkActivityIndicatorVisible = true
	}
	
	func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
		UIApplication.shared.isNetworkActivityIndicatorVisible = false
	}

}


