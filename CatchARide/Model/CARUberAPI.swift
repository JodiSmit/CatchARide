//
//  CARUberAPI.swift
//  CatchARide
//
//  Created by Jodi Lovell on 9/22/17.
//  Copyright © 2017 Smitty. All rights reserved.
//

import Foundation
import UberRides
import UberCore
import CoreLocation

//MARK: - This file manages API functions relating to Uber
class CARUberAPI: NSObject {
	
	
	let ridesClient = RidesClient()
	var error: NSError? = nil
	var pickupLocation: CLLocation? = nil
	var dropOffLocation: CLLocation? = nil
	var productName = ""
	
	func getUberEstimate(_ pickupCoordinates: CLLocationCoordinate2D, _ dropOffCoordinates: CLLocationCoordinate2D, completion: @escaping (_ result: Bool, _ error: NSError?) -> Void)  {
		let pickupLat = pickupCoordinates.latitude
		let pickupLon = pickupCoordinates.longitude
		pickupLocation = CLLocation(latitude: pickupLat, longitude: pickupLon)
		
		let dropOffLat = dropOffCoordinates.latitude
		let dropOffLon = dropOffCoordinates.longitude
		dropOffLocation = CLLocation(latitude: dropOffLat, longitude: dropOffLon)
		
		let currencyFormatter = NumberFormatter()
		currencyFormatter.usesGroupingSeparator = true
		currencyFormatter.numberStyle = NumberFormatter.Style.decimal
		
		var uberError: NSError? = nil
		
		ridesClient.fetchTimeEstimates(pickupLocation: pickupLocation!) { (estimatedTime, response) -> Void in
			
			if (response.error != nil) {
				self.error = NSError(domain: "", code: 10, userInfo: [NSLocalizedDescriptionKey : "We had a problem connecting to Uber. Please try again."])
				completion(false, self.error)
				return
			}
			if (estimatedTime.count == 0) {
				uberError = NSError(domain: "", code: 10, userInfo: [NSLocalizedDescriptionKey : "It looks like there are no Uber cars in your area. Please check that Uber operates in your region before trying your search again!"])
				completion(false, uberError)
				return
			}
			
			
			let timeCount = estimatedTime.count - 1
			var count = 0
			print("Uber timecount \(timeCount)")
			
			for item in estimatedTime {
				let id = item.productID
				let time = (item.estimate)!/60
				
				self.ridesClient.fetchPriceEstimates(pickupLocation: self.pickupLocation!, dropoffLocation: self.dropOffLocation!) { (priceEstimate, response) -> Void in
					print("Uber price count \(priceEstimate.count)")
					
					if (response.error != nil) {
						self.error = NSError(domain: "", code: 10, userInfo: [NSLocalizedDescriptionKey : "We had a problem connecting to Uber. Please try again."])
						completion(false, self.error)
						return
					}
					

					for item in priceEstimate {
						
						if item.lowEstimate != nil && item.highEstimate != nil {
							
							let price = (item.lowEstimate! + item.highEstimate!)/2
							
							if item.productID == id {
								self.ridesClient.fetchProduct(productID: id!) { (product, response) -> Void in
									if (response.error != nil) {
										self.error = NSError(domain: "", code: 10, userInfo: [NSLocalizedDescriptionKey : "We had a problem connecting to Uber. Please try again."])
										completion(false, self.error)
										return
									}
									self.productName = uberRideNames(productName: product!.name!)
									RidesResultTableViewController.rideEstimates.append(Rides(productName: self.productName, productId: item.productID!, timeEstimate: time, priceEstimate: price))
									
									if timeCount == count {
										print("Uber Completion True")
										completion(true,nil)
										return
									}
									
									count = count + 1
								}
							}
						}
					}
				}
			}
		}
	}
}


