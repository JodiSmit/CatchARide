//
//  RidesResultTableViewController.swift
//  CatchARide
//
//  Created by Jodi Lovell on 10/19/17.
//  Copyright © 2017 Smitty. All rights reserved.
//

import Foundation
import UIKit
import UberRides
import UberCore
import CoreLocation
import LyftSDK
import GoogleMobileAds

class RidesResultTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, GADBannerViewDelegate {
	
	var pickupLocation: CLLocation? = nil
	var dropOffLocation: CLLocation? = nil
	var rideKind = ""
	lazy var adBannerView: GADBannerView = {
		let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
		adBannerView.adUnitID = "ca-app-pub-7151213256097128/5003792435"
		//adBannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716" //TEST ADS
		adBannerView.delegate = self
		adBannerView.rootViewController = self
		
		return adBannerView
	}()
	
	@IBOutlet weak var sortButton: UIBarButtonItem!
	@IBOutlet weak var tableView: UITableView!
	
	static var rideEstimates = [Rides]()
	var sortedArray = [Rides]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.delegate = self
		tableView.dataSource = self
		sortedArray = RidesResultTableViewController.rideEstimates.sorted() {$0.priceEstimate < $1.priceEstimate}
		
		if UserDefaults.standard.bool(forKey: CARProducts.UnlockFullVersion) == true {
			tableView.tableHeaderView = nil
		} else {
			let adRequest = GADRequest()
			adRequest.testDevices = ["a0526a1ebb0446f4cd30e248afe7bed541242921"]
			adRequest.testDevices = [ kGADSimulatorID, "F706B123-CE03-4738-905D-90A726F733A7" ]
			adBannerView.load(GADRequest())
			let headerView: UIView = adBannerView
			tableView.tableHeaderView = headerView
		}
	}
	
	//MARK: - Buttons for sorting the results
	@IBAction func sortButton(_ sender: Any) {
		if (sortButton.title?.contains("Distance"))! {
			sortedArray = RidesResultTableViewController.rideEstimates.sorted() {$0.timeEstimate < $1.timeEstimate}
			DispatchQueue.main.async {
				self.tableView.reloadData()
				self.sortButton.title = "Sort by Price"
			}
		} else {
			sortedArray = RidesResultTableViewController.rideEstimates.sorted() {$0.priceEstimate < $1.priceEstimate}
			DispatchQueue.main.async {
				self.tableView.reloadData()
				self.sortButton.title = "Sort by Driver Distance"
			}
		}
	}
	
	@IBAction func backButton(_ sender: Any) {
		dismiss(animated: true, completion: nil)
		RidesResultTableViewController.rideEstimates.removeAll()
	}
	
	//MARK: - Tableview Configuration
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return RidesResultTableViewController.rideEstimates.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let rides = sortedArray[indexPath.row]
		let cell = tableView.dequeueReusableCell(withIdentifier: "ridesResultCell", for: indexPath) as! RidesTableViewCell
		if (rides.productName.contains("Uber")) {
			cell.productLogo.image = UIImage(named: "ios_rides-api_badge")
		} else if (rides.productName.contains("Lyft")) {
			cell.productLogo.image = UIImage(named: "wordmark-thin-pink")
		}
		
		cell.productName?.text = rides.productName
		cell.rideDetails.text = "\(rides.timeEstimate) mins away and approx $\(rides.priceEstimate)"
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let indexPath = tableView.indexPathForSelectedRow
		let rides = sortedArray[(indexPath?.row)!]
		rideKind = rides.productId
		var rideProvider = ""
		let currentCell = tableView.cellForRow(at: indexPath!)! as! RidesTableViewCell
		
		if (currentCell.productName?.text?.contains("Uber"))! {
			rideProvider = "Uber"
		} else {
			rideProvider = "Lyft"
		}
		
		let alertController = UIAlertController(title: "Open \(rideProvider)?", message: "Click OK to be redirected to the \(rideProvider) app to finish booking your ride.", preferredStyle: UIAlertControllerStyle.alert)
		let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
			return
		}
		let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
			if (currentCell.productName?.text?.contains("Uber"))!  {
				let rideParameters = self.deepLinkHelper()
				let deeplink = RequestDeeplink(rideParameters: rideParameters as! RideParameters, fallbackType: .appStore)
				deeplink.execute()
			} else if (currentCell.productName?.text?.contains("Lyft"))! {
				let pickup = self.pickupLocation?.coordinate
				let dropoff = self.dropOffLocation?.coordinate
				LyftDeepLink.requestRide(using: .native, kind: RideKind(rawValue: self.rideKind), from: pickup, to: dropoff)
				
			} else {
				print("Unknown ride error")
			}
		}
		alertController.addAction(okAction)
		alertController.addAction(cancelAction)
		
		self.present(alertController, animated: true, completion: nil)
	}
	
	//MARK: - Deeplink parameter builder function for Uber 
	func deepLinkHelper() -> NSObject {
		let builder = RideParametersBuilder()
		builder.pickupLocation = pickupLocation
		builder.dropoffLocation = dropOffLocation
		builder.productID = rideKind
		let rideParameters = builder.build()
		
		return rideParameters
	}
	
	func adViewDidReceiveAd(_ bannerView: GADBannerView) {
		print("Banner loaded successfully")
		
		// Reposition the banner ad to create a slide down effect
		let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
		bannerView.transform = translateTransform
		
		UIView.animate(withDuration: 0.5) {
			bannerView.transform = CGAffineTransform.identity
		}
	}
	
	func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
		print("Fail to receive ads")
		print(error)
		
	}
}
