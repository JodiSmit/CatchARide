//
//  AddressBookViewController.swift
//  CatchARide
//
//  Created by Jodi Lovell on 12/1/17.
//  Copyright © 2017 Smitty. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import GoogleMobileAds

class AddressBookViewController: UITableViewController, GADBannerViewDelegate {
	var addresses: [NSManagedObject] = []
	var dropOff = ""
	lazy var adBannerView: GADBannerView = {
		let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
		adBannerView.adUnitID = "ca-app-pub-7151213256097128/5810689215"
		//adBannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716" //TEST ADS
		adBannerView.delegate = self
		adBannerView.rootViewController = self
		
		return adBannerView
	}()
	
	
	@IBOutlet weak var editButton: UIBarButtonItem!
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
			return
		}
		let managedContext = appDelegate.persistentContainer.viewContext
		let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "AddressBook")
		
		do {
			addresses = try managedContext.fetch(fetchRequest)
		} catch let error as NSError {
			print("Could not fetch. \(error), \(error.userInfo)")
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		title = "Address Book"
		tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
		if UserDefaults.standard.bool(forKey: CARProducts.UnlockFullVersion) == true {
			tableView.tableHeaderView = nil
		} else {
			let adRequest = GADRequest()
			adRequest.testDevices = ["a0526a1ebb0446f4cd30e248afe7bed541242921"]
			adRequest.testDevices = [ kGADSimulatorID, "F706B123-CE03-4738-905D-90A726F733A7" ]
			adBannerView.load(GADRequest())
			let headerView: UIView = adBannerView
			tableView.tableHeaderView = headerView
		}
	}
	
	@IBAction func cancelButton(_ sender: Any) {
		dismiss(animated: true, completion: nil)
	}
	
	@IBAction func editButton(_ sender: Any) {
		if (self.tableView.isEditing) {
			editButton.title = "Edit"
			self.tableView.setEditing(false, animated: true)
		} else {
			editButton.title = "Done"
			self.tableView.setEditing(true, animated: true)
		}
	}
	
	
	//MARK: - Tableview Configuration
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return addresses.count
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let address = addresses[indexPath.row]
		var cell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath)
		if cell.detailTextLabel == nil {
			cell = UITableViewCell(style: .subtitle, reuseIdentifier: "addressCell")
		}
		cell.textLabel?.text = (address.value(forKeyPath: "name") as! String)
		cell.detailTextLabel?.text = (address.value(forKey: "address") as! String)
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let address = addresses[indexPath.row]
		let alertController = UIAlertController(title: "Search for Rides", message: "Click OK to search for rides to this destination!", preferredStyle: UIAlertControllerStyle.alert)

		let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
			self.dropOff = (address.value(forKey: "address") as! String)
			self.performSegue(withIdentifier: "afterSelectingSavedAddressViewController", sender: self)
		}
		
		let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
			return
		}
		
		alertController.addAction(okAction)
		alertController.addAction(cancelAction)
		
		self.present(alertController, animated: true, completion: nil)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "afterSelectingSavedAddressViewController",
			let searchVC = segue.destination as? SearchForRidesController {
			searchVC.dropOffAddress = dropOff
		}
	}
	
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		
		let managedContext = appDelegate?.persistentContainer.viewContext
		let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "AddressBook")
		
		if editingStyle == .delete {
			let address = addresses[indexPath.row]
			managedContext?.delete(address)
			do {
				try managedContext?.save()
				addresses = (try managedContext?.fetch(fetchRequest))!
			} catch let error as NSError {
				print("Could not fetch. \(error), \(error.userInfo)")
			}
			tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
		}
	}
	
	func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
		if (self.tableView.isEditing) {
			return UITableViewCellEditingStyle.delete
		}
		return UITableViewCellEditingStyle.none
	}
	
	func adViewDidReceiveAd(_ bannerView: GADBannerView) {
		print("Banner loaded successfully")
		
		// Reposition the banner ad to create a slide down effect
		let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
		bannerView.transform = translateTransform
		
		UIView.animate(withDuration: 0.5) {
			bannerView.transform = CGAffineTransform.identity
		}
	}
	
	func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
		print("Fail to receive ads")
		print(error)
		
	}
}
