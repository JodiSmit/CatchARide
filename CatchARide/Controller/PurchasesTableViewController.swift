//
//  PurchasesTableViewController.swift
//  CatchARide
//
//  Created by Jodi Lovell on 4/12/18.
//  Copyright © 2018 Smitty. All rights reserved.
//

/*
* Copyright (c) 2016 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import UIKit
import StoreKit

class PurchasesTableViewController: UITableViewController {
	
	var products = [SKProduct]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		title = "In App Purchases"
		
		refreshControl = UIRefreshControl()
		refreshControl?.addTarget(self, action: #selector(PurchasesTableViewController.reload), for: .valueChanged)
		
		let restoreButton = UIBarButtonItem(title: "Restore",
											style: .plain,
											target: self,
											action: #selector(PurchasesTableViewController.restoreTapped(_:)))
		let cancelButton = UIBarButtonItem(title: "Back",
										   style: .plain,
										   target: self,
										   action: #selector(PurchasesTableViewController.cancelPurchase(_:)))
		
		navigationItem.leftBarButtonItem = cancelButton
		navigationItem.rightBarButtonItem = restoreButton
		
		
		NotificationCenter.default.addObserver(self, selector: #selector(PurchasesTableViewController.handlePurchaseNotification(_:)),
											   name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification),
											   object: nil)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		reload()
	}
	
	@objc func reload() {
		products = []
		
		tableView.reloadData()
		
		CARProducts.store.requestProducts{success, products in
			if success {
				self.products = products!
				self.tableView.reloadData()
			} else {
				self.showAlert(AnyObject.self, title: "Nothing found", message: "Looks like there are no products here. Please try again later!")
			}
			
			self.refreshControl?.endRefreshing()
		}
	}
	
	@objc func restoreTapped(_ sender: AnyObject) {
		showActivityIndicator()
		CARProducts.store.restorePurchases()
	}
	
	@objc func cancelPurchase(_ sender: AnyObject) {
		self.dismiss(animated: true, completion: nil)
	}
	
	@objc func handlePurchaseNotification(_ notification: Notification) {
		guard let productID = notification.object as? String else { return }
		
		for (index, product) in products.enumerated() {
			guard product.productIdentifier == productID else { continue }
			tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
		}
	}
}

// MARK: - UITableViewDataSource

extension PurchasesTableViewController {
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return products.count
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! ProductCell
		
		let product = products[(indexPath as NSIndexPath).row]
		
		cell.product = product
		cell.buyButtonHandler = { product in
			self.showActivityIndicator()
			CARProducts.store.buyProduct(product)
		}
		
		return cell
	}
	
	func showAlert(_ sender: Any, title: String, message: String) {
		let errMessage = message
		let alert = UIAlertController(title: title, message: errMessage, preferredStyle: UIAlertControllerStyle.alert)
		alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
			let when = DispatchTime.now() + 1.5
			DispatchQueue.main.asyncAfter(deadline: when){
				alert.dismiss(animated: true, completion: nil)
			}
		}))
		self.present(alert, animated: true, completion: nil)
	}
}

